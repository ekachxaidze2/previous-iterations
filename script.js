// 1. თითოეული სტუდენტისთვის შექმნათ შესაბამისი მასივი და მოცემული ინფორმაცია ჩაწეროთ შიგნით
const students = [];

students[0] = {
    firstName: 'Jan',
    lastName: 'Reno',
    age: '26',
    scores: {
        javascript: 62,
        react: 57,
        python: 88,
        java: 90
    },
    gpas: {
        jsGpa: 1,
        reactGpa: 0.5,
        pythonGpa: 3,
        javaGpa: 3
    },
}
students[1] = {
    firstName: 'Klod',
    lastName: 'Mone',
    age: '19',
    scores: {
        javascript: 77,
        react: 52,
        python: 92,
        java: 67
    },
    gpas: {
        jsGpa: 2,
        reactGpa: 0.5,
        pythonGpa: 4,
        javaGpa: 1
    }
}
students[2] = {
    firstName: 'Van',
    lastName: 'Gogi',
    age: '21',
    scores: {
        javascript: 51,
        react: 98,
        python: 65,
        java: 70
    },
    gpas: {
        jsGpa: 0.5,
        reactGpa: 4,
        pythonGpa: 1,
        javaGpa: 1
    }
}
students[3] = {
    firstName: 'Dam',
    lastName: 'Squeari',
    age: '36',
    scores: {
        javascript: 82,
        react: 53,
        python: 80,
        java: 65
    },
    gpas: {
        jsGpa: 3,
        reactGpa: 0.5,
        pythonGpa: 2,
        javaGpa: 1
    }
}

const credits = {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3
}
let subjects = ['javascript', 'react', 'python', 'java']
// 2. თითოეული სტუდენტისთვის უნდა შეაგროვოთ (გამოთვალოთ) შემდეგი ინფორმაცია: ქულების ჯამი, ქულების საშუალო, GPA. ზემოთხსენებული მასივი ამ ინფორმაციასაც უნდა მოიცავდეს.

// sum
for(let i = 0; i < students.length; i++){
    students[i]['sum'] = 0
    for(let j = 0; j < subjects.length; j++){
       students[i]['sum'] += students[i].scores[subjects[j]]
    }
}

// avarage
for(let i = 0; i < students.length; i++){
    students[i]['avarage'] = 0
    for(let j = 0; j < subjects.length; j++){
       students[i]['avarage'] += (students[i].scores[subjects[j]]) / subjects.length 
    }
}

// gpa 
credits.total = 0;
let gpaArr = ['jsGpa','reactGpa', 'pythonGpa', 'javaGpa']
for(let i = 0; i < subjects.length; i++){
    credits.total += credits[subjects[i]]
}

for(let i =0; i < students.length; i++){
    students[i]['gpa'] = 0
    for(let j = 0; j < gpaArr.length; j++){
        students[i]['gpa'] += ((students[i].gpas[gpaArr[j]]) * credits[subjects[j]]) / credits.total
    }
}

// 3.გამოთვალეთ 4 - ვე სტუდენტის საშუალო არითმეტიკული (ანუ ვაფშე ყველა ქულა და ყველა საგანი), სტუდენტებს, რომელთა ცალკე საშუალო არითმეტიკული მეტი აქვთ ვიდრე საერთო არითმეტიკული, მივანიჭოთ "წითელი დიპლომის მქონეს" სტატუსი, ხოლო ვისაც საერთო საშუალოზე ნაკლები აქვს - "ვრაგ ნაროდა" სტატუსი

let avarageSum = 0
for(let i = 0; i < students.length; i++) {
    avarageSum += (students[i].avarage) / students.length
}
for(let i = 0; i < students.length; i++) {
    if(students[i].avarage > avarageSum){
        console.log(`${students[i].firstName} earned წითელი დიპლომი `)
    }else{
        console.log(`${students[i].firstName} vrag naroda`)
    }
}
// 4 უნდა გამოავლინოთ საუკეთესო სტუდენტი GPA ს მიხედვით

let maxGpa = students[0].gpa;
for(let i = 0; i < students.length; i++){
    if(students[i].gpa > maxGpa) {
       maxGpa = students[i].gpa
    }
}
console.log(maxGpa)

//  5.უნდა გამოავლინოთ საუკეთესო სტუდენტი 21 + ასაკში საშუალო ქულების მიხედვით (აქ 21+ იანის ხელით ამორჩევა არ იგულისხმება, უნდა შეამოწმოთ როგორც საშუალო ქულა, ასევე ასაკი)

let maxAvarage = students[0].avarage
for(let i = 0; i < students.length; i++){
    if(students[i].age > 21 && students[i].avarage > maxAvarage){
        maxAvarage = students[i].avarage
    }
}
console.log( maxAvarage)

 // 6. უნდა გამოავლინოთ სტუდენტი რომელიც საუკეთესოა ფრონტ-ენდის საგნებში საშუალო ქულების მიხედვით 
let frontEndSubj = ['javascript', 'react'];
for(let i = 0; i < students.length; i++){
    students[i]['frontAvarage'] = 0;
    for(let j = 0; j < frontEndSubj.length; j++){
        students[i]['frontAvarage'] += (students[i].scores[frontEndSubj[j]]) / frontEndSubj.length
    }
    console.log(students[i])
}

let frontMaxAvarage = students[0].frontAvarage
for(let i = 0; i < students.length; i++){   
    if(students[i].frontAvarage > frontMaxAvarage){
        frontMaxAvarage = students[i].frontAvarage
    }
}
console.log(frontMaxAvarage)

